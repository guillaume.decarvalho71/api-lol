# syntax=docker/dockerfile:1


FROM openjdk:14


WORKDIR /app
COPY .gradle/ .gradle
COPY gradle/ gradle
COPY gradlew.bat  settings.gradle build.gradle gradlew ./

RUN ./gradlew build

COPY src ./src

CMD ["java", "Main"]