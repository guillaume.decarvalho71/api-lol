package org.example.apilol.client;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.IOException;

public class HttpBuilder {
    private static final String TOKEN_API = "RGAPI-17da9d9d-ec84-49a8-9480-6811e6ac0843";
    private static final String X_RIOT_TOKEN = "X-Riot-Token";
    private static Retrofit.Builder builder;
    private static OkHttpClient.Builder httpClient;

    public static Retrofit HttpBuilder(String baseUrl){
        builder=new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());
        httpClient = new OkHttpClient.Builder();

        SetInterceptor();
        builder.client(httpClient.build());
        return builder.build();
    }

    private static void SetInterceptor() {
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder().header(X_RIOT_TOKEN, TOKEN_API);
                Request request = builder.build();
                return chain.proceed(request);
            }
        });
    }
}
