package org.example.apilol.client;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.example.apilol.client.api.Summoner;
import org.example.apilol.client.model.SummonerDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.IOException;

public class Main {

    public static final String TOKEN_API = "RGAPI-17da9d9d-ec84-49a8-9480-6811e6ac0843";

    public static void main(String... args) {

        Retrofit.Builder builder=new Retrofit.Builder().baseUrl("https://euw1.api.riotgames.com/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());
       OkHttpClient.Builder httpClient = new OkHttpClient.Builder();



       httpClient.addInterceptor(new Interceptor() {
           @Override
           public Response intercept(Chain chain) throws IOException {
               Request original = chain.request();
               Request.Builder builder = original.newBuilder().header("X-Riot-Token", TOKEN_API);
               Request request = builder.build();
               return chain.proceed(request);
           }
       });
        builder.client(httpClient.build());
        Retrofit retrofit=builder.build();
        Summoner summoner = retrofit.create(Summoner.class);

        Call<SummonerDTO> summoner1 = summoner.getSummonerByEncryptedSummonerId("NnHoFEfIH555RtB_VB1zdvr4VILobB8tznihSIWa_fjs7as");
        summoner1.enqueue(new Callback<SummonerDTO>() {
            @Override
            public void onResponse(Call<SummonerDTO> call, retrofit2.Response<SummonerDTO> response) {
                System.out.println(response.body());
                System.exit(0);
            }

            @Override
            public void onFailure(Call<SummonerDTO> call, Throwable t) {

            }
        });

    }
}
