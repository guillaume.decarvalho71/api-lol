package org.example.apilol.client.api;

import org.example.apilol.client.model.AccountDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Account {
    @GET("/riot/account/v1/accounts/by-puuid/{puuid}")
    public Call<AccountDTO>getAccountBypuuid(@Path("puuid") String puuid);

    @GET("/riot/account/v1/accounts/by-riot-id/{gameName}/{tagLine}")
    public Call<AccountDTO>getAccountByRiotId(@Path("gameName") String gameName,@Path("tagLine") String tagLine);
    @GET("/riot/account/v1/active-shards/by-game/{game}/by-puuid/{puuid}")
    public Call<String>getAccountByGame(@Path("game") String game,@Path("puuid") String puuid);

}
