package org.example.apilol.client.api;

import org.example.apilol.client.model.ChampionMasteryDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface ChampionMastery {
    @GET("/lol/champion-mastery/v4/champion-masteries/by-summoner/{encryptedSummonerId}")
    Call<List<ChampionMasteryDto>> getChampionMasteryByeEncryptedSummonerId(@Path("encryptedSummonerId") String  encryptedSummonerId);
    @GET("/lol/champion-mastery/v4/champion-masteries/by-summoner/{encryptedSummonerId}/by-champion/{championId}")
    Call<List<ChampionMasteryDto>> getChampionMasteryByeEncryptedSummonerIdAndChampionId(@Path("encryptedSummonerId") String  encryptedSummonerId,@Path("championId") String  championId);
}
