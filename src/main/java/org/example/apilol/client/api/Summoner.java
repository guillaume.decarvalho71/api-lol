package org.example.apilol.client.api;

import org.example.apilol.client.model.SummonerDTO;
import retrofit2.Call;
import retrofit2.http.*;

public interface Summoner {
    @GET("lol/summoner/v4/summoners/by-name/{summonerName}")
    public  Call<SummonerDTO> getSummoner(@Path("summonerName") String summonerName);

    @GET("lol/summoner/v4/summoners/by-puuid/{encryptedPUUID}")
    public  Call<SummonerDTO> getSummonerByPuuid(@Path("encryptedPUUID") String encryptedPUUID);
    @GET("lol/summoner/v4/summoners/{encryptedSummonerId}")
    public  Call<SummonerDTO> getSummonerByEncryptedSummonerId(@Path("encryptedSummonerId") String encryptedSummonerId);
}
