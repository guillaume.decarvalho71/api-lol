package org.example.apilol.client.model;

public class ChampionMasteryDto {
    long championPointsUntilNextLevel;		//Number of points needed to achieve next level. Zero if player reached maximum champion level for this champion.
    boolean   chestGranted;		//Is chest granted for this champion or not in current season.
    long championId;		//Champion ID for this entry.
    long       lastPlayTime	;//	Last time this champion was played by this player - in Unix milliseconds time format.
    int        championLevel;	//int	Champion level for specified player and champion combination.
    String summonerId;	//Summoner ID for this entry. (Encrypted)
    int championPoints;	//	Total number of champion points for this player and champion combination - they are used to determine championLevel.
    long championPointsSinceLastLevel;//		Number of points earned since current level has been achieved.
    int        tokensEarned;	//The token earned for this champion at the current championLevel. When the championLevel is advanced the tokensEarned resets to 0.

    public long getChampionPointsUntilNextLevel() {
        return championPointsUntilNextLevel;
    }

    public void setChampionPointsUntilNextLevel(long championPointsUntilNextLevel) {
        this.championPointsUntilNextLevel = championPointsUntilNextLevel;
    }

    public boolean isChestGranted() {
        return chestGranted;
    }

    public void setChestGranted(boolean chestGranted) {
        this.chestGranted = chestGranted;
    }

    public long getChampionId() {
        return championId;
    }

    public void setChampionId(long championId) {
        this.championId = championId;
    }

    public long getLastPlayTime() {
        return lastPlayTime;
    }

    public void setLastPlayTime(long lastPlayTime) {
        this.lastPlayTime = lastPlayTime;
    }

    public int getChampionLevel() {
        return championLevel;
    }

    public void setChampionLevel(int championLevel) {
        this.championLevel = championLevel;
    }

    public String getSummonerId() {
        return summonerId;
    }

    public void setSummonerId(String summonerId) {
        this.summonerId = summonerId;
    }

    public int getChampionPoints() {
        return championPoints;
    }

    public void setChampionPoints(int championPoints) {
        this.championPoints = championPoints;
    }

    public long getChampionPointsSinceLastLevel() {
        return championPointsSinceLastLevel;
    }

    public void setChampionPointsSinceLastLevel(long championPointsSinceLastLevel) {
        this.championPointsSinceLastLevel = championPointsSinceLastLevel;
    }

    public int getTokensEarned() {
        return tokensEarned;
    }

    public void setTokensEarned(int tokensEarned) {
        this.tokensEarned = tokensEarned;
    }

    @Override
    public String toString() {
        return "ChampionMasteryDto{" +
                "championPointsUntilNextLevel=" + championPointsUntilNextLevel +
                ", chestGranted=" + chestGranted +
                ", championId=" + championId +
                ", lastPlayTime=" + lastPlayTime +
                ", championLevel=" + championLevel +
                ", summonerId='" + summonerId + '\'' +
                ", championPoints=" + championPoints +
                ", championPointsSinceLastLevel=" + championPointsSinceLastLevel +
                ", tokensEarned=" + tokensEarned +
                '}';
    }
}
